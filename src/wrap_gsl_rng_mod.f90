!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!   wrap_gsl_rng_mod - fortran wrapper for bits of GNU Scientific Library !
!                                                                         !
!   Copyright (C) 2016-2019 by Boud Roukema                               !
!   boud cosmo.torun.pl                                                   !
!                                                                         !
!   This program is free software; you can redistribute it and/or modify  !
!   it under the terms of the GNU General Public License as published by  !
!   the Free Software Foundation; either version 2 of the License, or     !
!   (at your option) any later version.                                   !
!                                                                         !
!   This program is distributed in the hope that it will be useful,       !
!   but WITHOUT ANY WARRANTY; without even the implied warranty of        !
!   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         !
!   GNU General Public License for more details.                          !
!                                                                         !
!   You should have received a copy of the GNU General Public License     !
!   along with this program; if not, write to the                         !
!   Free Software Foundation, Inc.,                                       !
!   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA              !
!   or see https://www.gnu.org/licenses/licenses.html#GPL .               !
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

module wrap_gsl_rng_mod
  ! see e.g. https://stackoverflow.com/tags/fortran-iso-c-binding/info

  interface
     subroutine get_gsl_rng_seeds &
          (iseed, MaxRandNumStreams, seeds) &
          bind(C, name='get_gsl_rng_seeds')
       use iso_C_binding
       implicit none
       ! INPUTS
       integer (c_int64_t), value :: iseed
       integer (c_int32_t), value :: MaxRandNumStreams
       ! OUTPUT
       integer (c_int64_t), dimension(MaxRandNumStreams) :: seeds
     end subroutine get_gsl_rng_seeds
  end interface

  interface
     subroutine gauss_gsl &
          (Seed, RandNum, free_gauss_gsl) &
          bind(C, name='gauss_gsl')
       use iso_C_binding
       implicit none
       integer (c_int64_t), value :: Seed ! pass by value
       real (c_double) :: RandNum ! pass by reference
       integer (c_int), value :: free_gauss_gsl ! pass by value
     end subroutine gauss_gsl
  end interface

end module wrap_gsl_rng_mod
