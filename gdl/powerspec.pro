;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;   Copyright (C) 2008-2016 by Simon Prunet                               ;
;   prunet iap.fr                                                         ;
;                                                                         ;
;   This program is free software; you can redistribute it and/or modify  ;
;   it under the terms of the GNU General Public License as published by  ;
;   the Free Software Foundation; either version 2 of the License, or     ;
;   (at your option) any later version.                                   ;
;                                                                         ;
;   This program is distributed in the hope that it will be useful,       ;
;   but WITHOUT ANY WARRANTY; without even the implied warranty of        ;
;   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         ;
;   GNU General Public License for more details.                          ;
;                                                                         ;
;   You should have received a copy of the GNU General Public License     ;
;   along with this program; if not, write to the                         ;
;   Free Software Foundation, Inc.,                                       ;
;   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA              ;
;   or see https://www.gnu.org/licenses/licenses.html#GPL .               ;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

pro powerspec, filename, k, pk

read_grafic,filename,nn,cosmo,cube

fcube = fft(cube,-1)
fcube = double(fcube*conj(fcube))
dx=cosmo(0)
n=nn(0)
dk=2.0*!pi/(n*dx)
h0=cosmo(7)

a=findgen(n/2)
kvec=[findgen(n/2+1),-reverse(a(1:*))]*dk

kx=fltarr(n,n,n)
ky=kx
kz=kx
for i=0,n-1 do begin
    for j=0,n-1 do begin
        kx(*,i,j) = kvec
    endfor
endfor

for i=0,n-1 do begin
    for j=0,n-1 do begin
        ky(i,*,j) = kvec
    endfor
endfor

for i=0,n-1 do begin
    for j=1,n-1 do begin
        kz(i,j,*) = kvec
    endfor
endfor

k = dblarr(n/2)
pk = dblarr(n/2)
modk = sqrt(kx^2+ky^2+kz^2)
for i=0,n/2-1 do begin
    ou = where(modk ge double(i)*dk and modk lt double(i+1)*dk)
    pk(i) = total(fcube(ou),/double)/n_elements(ou)*(h0/100.)^3
    k(i) = total(modk(ou),/double)/n_elements(ou)/(h0/100.)
endfor

end
