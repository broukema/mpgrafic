;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;   Copyright (C) 2008-2016 by Simon Prunet                               ;
;   prunet iap.fr                                                         ;
;                                                                         ;
;   This program is free software; you can redistribute it and/or modify  ;
;   it under the terms of the GNU General Public License as published by  ;
;   the Free Software Foundation; either version 2 of the License, or     ;
;   (at your option) any later version.                                   ;
;                                                                         ;
;   This program is distributed in the hope that it will be useful,       ;
;   but WITHOUT ANY WARRANTY; without even the implied warranty of        ;
;   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         ;
;   GNU General Public License for more details.                          ;
;                                                                         ;
;   You should have received a copy of the GNU General Public License     ;
;   along with this program; if not, write to the                         ;
;   Free Software Foundation, Inc.,                                       ;
;   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA              ;
;   or see https://www.gnu.org/licenses/licenses.html#GPL .               ;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

pro write_grafic, filename, nn, cosmo, cube

openw,1,filename,/f77

writeu,1,nn,cosmo
tempo=fltarr(nn(0),nn(1))

for i=0,nn(2)-1 do begin

    tempo = cube(*,*,i)
    writeu,1,tempo

endfor
close,1
end
