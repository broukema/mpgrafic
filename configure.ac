AC_INIT([mpgrafic], [0.3.19])

#AC_CONFIG_SRCDIR([src/mpgrafic.f90])
AM_INIT_AUTOMAKE
AM_CONFIG_HEADER(config.h)

AC_ARG_ENABLE([devmode],
  [  --enable-devmode   Compiler options for development only],
  [case "${enableval}" in
    yes) devmode=true ;;
    no)  devmode=false ;;
    *) AC_MSG_ERROR([bad value ${enableval} for --enable-devmode]) ;;
  esac],[devmode=false])
# AM_CONDITIONAL([DEV_MODE], [test 1 = 1 ]) # to hardwire
AM_CONDITIONAL([DEV_MODE], [test x${devmode} = xtrue ])


# See https://lintian.debian.org/tags/binary-file-built-without-LFS-support.html
# and https://stackoverflow.com/questions/9073667/where-to-find-the-complete-definition-of-off-t-type;
# these apply to src/parallel_io.c which handles big files; present solution
# is to use int64_t
AC_SYS_LARGEFILE

AC_LANG_FORTRAN77
# MPI stuff
AC_PROG_FC
AC_ARG_VAR(MPIFC,[MPI Fortran compiler command])
AC_CHECK_PROGS(MPIFC, mpifort mpif90 mpxlf95_r mpxlf90_r mpxlf95 mpxlf90 mpf90 cmpif90c, $FC)
FC="$MPIFC"
F77="$FC"
AC_SUBST(MPIFC)

# needs the minimal test file mpi_use_f08_detect.f90:
#program mpi_use_f08_detect
#  use mpi_f08
#  call MPI_INIT(ierr)
#  call MPI_FINALIZE(ierr)
#end program mpi_use_f08_detect

if $($MPIFC mpi_use_f08_detect.f90); then
        DHAVE_USE_MPI_F08=-DHAVE_USE_MPI_F08
else
        DHAVE_USE_MPI_F08=-UHAVE_USE_MPI_F08
fi
AC_SUBST(DHAVE_USE_MPI_F08)

AC_FC_LIBRARY_LDFLAGS


# Precision
AC_ARG_VAR(PRECISION,[Single or double precision calculations])
AC_ARG_ENABLE([single],[AS_HELP_STRING([--enable-single],
        [use single precision (default is double)])],
        [use_single=yes],
        [use_single=no])
AC_ARG_VAR(FFTWPREC,[fftw precision prefix])

if test x$use_single = xyes; then
        AC_CHECK_LIB(sfftw,fftw3d_f77_create_plan,[
        FFTWPREC=s
        PRECISION=DOUB
        ],[
          AC_CHECK_LIB(fftw,fftw3d_f77_create_plan,[
          FFTWPREC=
          PRECISION=SING
          ],[
          echo "Error! You need to have the (single precision) fftw library."
          exit -1
          ])
        ])
else
        AC_CHECK_LIB(dfftw,fftw3d_f77_create_plan,[
        FFTWPREC=d
        PRECISION=DOUB
        ],[
          AC_CHECK_LIB(fftw,fftw3d_f77_create_plan,[
          FFTWPREC=
          PRECISION=DOUB
          ],[
          echo "Error! You need to have the (double precision) fftw library."
          exit -1
          ])
        ])
fi
AC_SUBST(FFTWPREC)
AC_SUBST(PRECISION)

AC_LANG_C
AC_PROG_CC

AC_CHECK_LIB(gcc,main,,
        [AC_MSG_WARN(Could not find libgcc)
        ])

AC_CHECK_LIB(gslcblas,cblas_dgemv)
AC_CHECK_LIB(gsl,gsl_spline_init,,
        [AC_MSG_ERROR(Could not find gsl. Hint (1)  find /usr -name "*gsl*" -print   Hint (2)  install gsl*.a  e.g. from  http://www.gnu.org/software/gsl/ and put it in /usr/local/lib/ or set environment variable LDFLAGS to be -L/my/directory/lib/ with the library and CFLAGS to be -I/my/directory/include for the include files.)
        ])

# needed for rfftw3d_mpi_create_plan_c_wrap.c
AX_MPI
CC="$MPICC"
AC_SUBST(MPICC)

AC_CONFIG_FILES([Makefile src/Makefile])
AC_OUTPUT
